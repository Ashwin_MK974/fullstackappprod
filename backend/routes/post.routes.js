const express = require("express");
const {
  setPosts,
  getPosts,
  editPost,
  deletePosts,
  likePosts,
  dislikePosts,
} = require("../controllers/posts.controllers");

const router = express.Router();

router.get("/", getPosts);

router.post("/", setPosts);

router.put("/:id", editPost);

router.delete("/:id", deletePosts);

router.patch("/like-post/:id", likePosts);

router.patch("/dislike-post/:id", dislikePosts);
module.exports = router;
