import React, { useEffect, useState } from "react";
import axios from "axios";
import Post from "./Post";
import { useDispatch, useSelector } from "react-redux";
import { getPosts } from "../feature/post.slice";

const Thread = () => {
  const posts = useSelector((state) => state.posts.postsData);
  const dispatch = useDispatch();
  useEffect(() => {
    //axios.get("http://localhost:5000/post").then((res) => {
    //console.log(res.data);
    //dispatch(getPosts(res.data));
    //setPosts(res.data);
    //});
    dispatch(getPosts()); //Pour obtenir les Posts via Axios définie dans le slice
  }, []);
  return (
    <div className="thread-container">
      {posts &&
        posts
          .slice()
          .sort((a, b) => b.createdAt.localeCompare(a.createdAt))
          .map((post) => (
            <Post key={post._id} post={post} /> //Ici on fais du props Dreading c'est a dire que userId n'a pas d'autre utilisation que de ce faire passer à un autre argument.
          ))}
    </div>
  );
};

export default Thread;
//<Post key={post._id} post={post} userId={userId} />  Ancienne version de passer des données.
