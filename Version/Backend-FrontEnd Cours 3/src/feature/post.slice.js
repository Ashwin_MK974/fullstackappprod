import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
export const getPosts = createAsyncThunk("getPosts", async (_, thunkAPI) => {
  axios.get("http://localhost:5000/post/").then((res) => {
    console.log("from post.slice", res.data);
    thunkAPI.dispatch(getPostsSuccess(res.data));
  });
});
const postSlice = createSlice({
  name: "posts",
  initialState: {
    postsData: [],
  },
  reducers: {
    getPostsSuccess: (state, { payload }) => {
      state.postsData = payload;
    },
    createPosts: (state, { payload }) => {
      state.postsData.push(payload);
    },
    editPosts: (state, { payload }) => {
      state.postsData = state.postsData.map((posts) => {
        if (posts._id == payload[0]) {
          return {
            ...posts,
            message: payload[1],
          };
        } else {
          return posts;
        }
      });
    },
    deletePosts: (state, { payload }) => {
      state.postsData = state.postsData.filter((post) => post._id != payload);
    },
    likePosts: (state, { payload }) => {
      state.postsData = state.postsData.map((post) => {
        if (post._id == payload[0]) {
          return { ...post, likers: [...post.likers, payload[1]] };
        } else {
          return post;
        }
      });
    },
    dislikePosts: (state, { payload }) => {
      state.postsData = state.postsData.map((post) => {
        if (post._id == payload[0]) {
          return {
            ...post,
            likers: post.likers.filter((likers) => likers != payload[1]),
          };
        } else {
          return post;
        }
      });
    },
  },
});

export const {
  getPostsSuccess,
  createPosts,
  editPosts,
  deletePosts,
  likePosts,
  dislikePosts,
} = postSlice.actions;
export default postSlice.reducer;
