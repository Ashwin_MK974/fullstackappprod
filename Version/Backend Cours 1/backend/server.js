const express = require("express");
const connectDB = require("./config/db");
const app = express();

/* app.get("/post", (req, res) => {
  res.json({ message: "Voici  les données" });
}); */

//Middleware pour traiter les donnée de la request
app.use(express.json());
app.use(express.urlencoded());
//Connexion à la databse
connectDB();
app.use("/post", require("./routes/post.routes"));

const port = 5000;
app.listen(port, () => {
  console.log("Le serveur à démarré au port " + port);
});
