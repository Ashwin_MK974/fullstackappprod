const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const connectDB = async () => {
  try {
    mongoose.set("strictQuery", false);
    mongoose.connect(process.env.MONGO_URI);
    console.log("Mongo Connecté");
  } catch (e) {
    console.log("Une erreur est survenue : ", e);
    process.exit();
  }
};

module.exports = connectDB;
