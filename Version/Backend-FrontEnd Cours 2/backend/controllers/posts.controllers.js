const { json } = require("express");
const PostModel = require("../models/post.model");

module.exports.setPosts = async (req, res) => {
  if (req.body.message == undefined) {
    res.json({
      message: "Une erreur est survenue : Veuillez ajouter un message",
    });
  } else {
    const post = await PostModel.create({
      message: req.body.message,
      author: req.body.author,
    });
    res.status(200).json(post);
  }
};

module.exports.getPosts = async (req, res) => {
  const post = await PostModel.find();
  res.status(200).json(post);
};

module.exports.deletePosts = async (req, res) => {
  const post = await PostModel.findByIdAndDelete(req.params.id);
  res.status(200).json({ message: "Message supprimer : " + req.params.id });
};

module.exports.editPost = async (req, res) => {
  const post = await PostModel.findById(req.params.id);
  if (!post) {
    res.status(400).json({ message: "Ce poste n'existe pas !" });
  }

  const updatePost = await PostModel.findByIdAndUpdate(post, req.body, {
    new: true,
  });

  res.status(200).json(updatePost);
};

module.exports.likePosts = async (req, res) => {
  try {
    const post = await PostModel.findByIdAndUpdate(
      req.params.id,
      { $addToSet: { likers: req.body.userId } },
      { new: true }
    );
    res.status(200).json({ message: "Poste Liké par  " + req.body.userId });
  } catch {
    res.status(400).json(res);
  }
};

module.exports.dislikePosts = async (req, res) => {
  try {
    const post = await PostModel.findByIdAndUpdate(
      req.params.id,
      { $pull: { likers: req.body.userId } },
      { new: true }
    );
    res.status(200).json({ message: "Poste DisLiké par  " + req.body.userId });
  } catch {
    res.status(400).json(res);
  }
};
