import React, { useState } from "react";
import axios from "axios";
const NewPost = ({ userId }) => {
  const [message, setMessage] = useState("");
  const handleForm = (e) => {
    e.preventDefault();
    const data = { message, author: userId }; //Equivalent à x-www-form-urlencoded
    axios
      .post("http://localhost:5000/post", data)
      .then((res) => console.log(res));
    setMessage("");
  };
  return (
    <form
      action=""
      className="new-post-container"
      onSubmit={(e) => {
        handleForm(e);
      }}
    >
      <textarea
        placeholder="Quoi de neuf ?"
        onChange={(e) => setMessage(e.target.value)}
        value={message}
      ></textarea>
      <input type="submit" value="Envoyer" />
    </form>
  );
};

export default NewPost;
