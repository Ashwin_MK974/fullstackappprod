import axios from "axios";
import React, { useEffect, useState } from "react";

const DeletePost = ({ postId }) => {
  const handleDelete = () => {
    axios.delete("http://localhost:5000/post/" + postId);
  };
  return (
    <span
      id="delete-btn"
      onClick={(_) => {
        handleDelete();
      }}
    >
      &#10010;
    </span>
  );
};

export default DeletePost;
