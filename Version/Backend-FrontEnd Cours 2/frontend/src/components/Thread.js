import React, { useEffect, useState } from "react";
import axios from "axios";
import Post from "./Post";

const Thread = ({ userId }) => {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:5000/post").then((res) => {
      //console.log(res.data);
      setPosts(res.data);
    });
  }, []);
  return (
    <div className="thread-container">
      {posts
        .sort((a, b) => b.createdAt.localeCompare(a.createdAt))
        .map((post) => (
          <Post key={post._id} post={post} userId={userId} /> //Ici on fais du props Dreading c'est a dire que userId n'a pas d'autre utilisation que de ce faire passer à un autre argument.
        ))}
    </div>
  );
};

export default Thread;
