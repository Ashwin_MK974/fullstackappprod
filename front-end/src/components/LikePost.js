import React, { useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { dislikePosts, likePosts } from "../feature/post.slice";

const LikePost = ({ post }) => {
  const [userLiked, setUserLiked] = useState(false);
  const userId = useSelector((state) => state.user.userId);
  const dispatch = useDispatch();
  useEffect(() => {
    if (post.likers) {
      if (post.likers.includes(userId)) {
        setUserLiked(true);
      } else {
        setUserLiked(false);
      }
      console.log(userLiked);
    }
  }, [userId]);
  const likePost = () => {
    axios.patch("http://localhost:5000/post/like-post/" + post._id, {
      userId: userId,
    });
    dispatch(likePosts([post._id, userId]));
  };
  const dislikePost = () => {
    axios.patch("http://localhost:5000/post/dislike-post/" + post._id, {
      userId: userId,
    });
    dispatch(dislikePosts([post._id, userId]));
  };
  return (
    <div className="like-icon">
      <p>{post.likers ? post.likers.length : 0}</p>
      {userLiked ? (
        <span
          id="like-btn"
          onClick={() => {
            dislikePost();
            setUserLiked(false);
          }}
        >
          &#9829;
        </span>
      ) : (
        <span
          id="dislike-btn"
          onClick={() => {
            likePost();
            setUserLiked(true);
          }}
        >
          &#9829;
        </span>
      )}
    </div>
  );
};

export default LikePost;
