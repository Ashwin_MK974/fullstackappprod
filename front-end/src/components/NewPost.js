import React, { useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { createPosts } from "../feature/post.slice";
import { getPosts } from "../feature/post.slice";
const NewPost = () => {
  const [message, setMessage] = useState("");
  const userId = useSelector((state) => state.user.userId);
  const dispatch = useDispatch();
  const handleForm = (e) => {
    e.preventDefault();
    console.log("User ID =====> : " + userId);
    const data = { message, author: userId }; //Equivalent à x-www-form-urlencoded
    axios.post("http://localhost:5000/post", data).then((res) => {
      console.log(res);
      dispatch(createPosts(data)); //On créer le POST sans l'ID via Redux
      //Get Posts afin de pouvoir obtenir l'ID
      dispatch(getPosts());
    });

    setMessage("");
  };
  return (
    <form
      action=""
      className="new-post-container"
      onSubmit={(e) => {
        handleForm(e);
      }}
    >
      <textarea
        placeholder="Quoi de neuf ?"
        onChange={(e) => setMessage(e.target.value)}
        value={message}
      ></textarea>
      <input type="submit" value="Envoyer" />
    </form>
  );
};

export default NewPost;
